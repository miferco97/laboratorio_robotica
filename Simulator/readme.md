# README #

It is required to install rosbridge:
* ```$ sudo apt-get install ros-<rosdistro>-rosbridge-server```

### How to run Simulation ###

* First launch rosbridge server ```$ roslaunch rosbridge_server rosbridge_websocket```
* Then run simulation ```$ ./linux.x86_64```
* Simulation publishes in three topics:
	* camara -> sensor_msgs/CompressedImage 
	* angle -> std_msgs/Float32 (Angle in degrees)
	* laser -> std_msgs/Float32 (Distance to the ball from one side)
* It subscribes to next topic:
	* control -> std_msgs/Float32 (Torque applied)

### Manual controls ###

* Left Arrow Key to apply +50 torque (while key is pressed)
* Right Arrow Key to apply -50 torque (while key is pressed)
* Press R to restart simulation
