#include "ros/ros.h"
#include "std_msgs/Float32.h"
#include <dynamic_reconfigure/server.h>

#include <iostream>

#include "controller.hpp"



float laser_measure = 0.0f;
float dist_ref = 0.0f;
float last_laser_measure = 0.0f;
bool reseted = false;
std_msgs::Float32 servo_position;
void CallbackReset(const std_msgs::Float32& msg){
    if (msg.data == 1)reseted = true;
}

void CallbackDistRef(const std_msgs::Float32& msg){
    dist_ref = msg.data;
}

void CallbackLaserMeasure(const std_msgs::Float32& msg){
    laser_measure = msg.data;
    if (msg.data >= 50) laser_measure = 0.0f;  
    laser_measure = - (laser_measure -  25);
    // laser_measure = laser_measure*0.1 + last_laser_measure *0.9;
    // last_laser_measure = laser_measure;
        std::cout <<"Laser Measure = " << laser_measure << std::endl;
}

   
int main(int argc, char **argv)
{
    ros::init(argc, argv,"Controller_node");
    ros::NodeHandle n;
    ros::Subscriber sub_laser = n.subscribe("/laser", 1, &CallbackLaserMeasure);
    ros::Subscriber sub_dist = n.subscribe("/dist_ref", 1, &CallbackDistRef);
    ros::Subscriber sub_reset = n.subscribe("/reset", 1, &CallbackReset);
    // ros::Publisher pub_servo = n.advertise<std_msgs::Float32>("/servo", 1);
    ros::Publisher pub_servo = n.advertise<std_msgs::Float32>("/control", 1);


    PIDController my_controller;
    
    #ifdef DYNAMIC_TUNING

        //dynamic Reconfigure
        dynamic_reconfigure::Server<BasicController::ControllerConfig> server;
        dynamic_reconfigure::Server<BasicController::ControllerConfig>::CallbackType f;

        f = boost::bind(&PIDController::parametersCallback, &my_controller,_1, _2);
        server.setCallback(f);

    #endif


    
    my_controller.setSaturation(-30.0,30.0f);
    // my_controller.setCenteredBias();

    auto t0 = ros::Time::now(); 
    ros::Rate rate(20);
    while(ros::ok())
    {
        //updating all the ros msgs
        ros::spinOnce();
        // running the localizer
        if (reseted){
            reseted = false;
            my_controller.reset();
        }

        auto d_t = ros::Time::now()-t0;
        auto delta_t = d_t.toSec();
        // std::cout <<"dt: " <<  delta_t<< std::endl;
        float output = my_controller.computePID(laser_measure,dist_ref,delta_t);
        // std::cout <<"output" <<  output<< std::endl;
        servo_position.data = output;
        // std::cout <<"data published " <<  servo_position.data<< std::endl;
        pub_servo.publish(servo_position);
        t0 = ros::Time::now();
        
        rate.sleep();
    }

    return 0;
}
    