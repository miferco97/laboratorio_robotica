#ifndef __CONTROLLER_H__
#define __CONTROLLER_H__

#include "ros/ros.h"
#include "std_msgs/Float32MultiArray.h"
// #include <Eigen/Dense>


#include <iostream>
#include <vector>
#include <math.h>
#include <array>

#define DEBUG -1
#define DYNAMIC_TUNING
#include <BasicController/ControllerConfig.h>

class PIDController{

private:

    
    float Kp_ = 0.5f;
    float Ki_ = 0.0;
    float Kd_ = 0.0;

    float Kp_int_ = -2.0f;
    float Ki_int_ = 0.0;
    float Kd_int_ = 0.0;
    

    float internalRef;
    // float internalMeasure = 0.0f;
    float accum_error_internal_ = 0.0f;
    float last_error_ = 0.0f;
    // float ref_ = 0.0f;

    bool has_saturation_ = false;
    float saturation_high_value_;
    float saturation_low_value_;
    float last_d_error_ = 0.0;

    float last_speed_ =0.0f, last_position_ = 0.0f;
    ros::Time reset_time_;

    float bias_ = 0.0f;

private:

    float internalPID(float measure, float ref , float dt){
        float error = measure - ref;
        float d_error = (error -last_error_)/dt;
        d_error = d_error*0.1+ last_d_error_ *0.9;
        last_d_error_ = d_error;
        float output  = Kp_ * error + Ki_ * accum_error_internal_  -Kd_ * d_error;
        accum_error_internal_ += error;
        last_error_ = error;
        return output;
     }

public:
    
    PIDController(){
        reset_time_ = ros::Time::now();
    };
    // PIDController(){};
    void reset(){
        last_d_error_  = 0.0;
        last_speed_    = 0.0f;
        last_position_ = 0.0f;
        reset_time_ = ros::Time::now();
    }

    float getSpeed(float position,float dt){
        float speed =(position - last_position_)/dt;
        last_position_ = position;
        speed = speed * 0.1 + last_speed_* 0.9;
        last_speed_ = speed;
        return speed;

    }

    float computePID(float measure , float ref,float dt){
        float speed_measure = getSpeed(measure,dt);
        float error = measure - ref;
        float d_error = (error -last_error_)/dt;
        float int_ref = Kp_int_ * error + Kd_int_*d_error;
        
        float output = internalPID(speed_measure,int_ref,dt);
        
        output += bias_;

        // Saturate output
        if (has_saturation_){
            if (output > saturation_high_value_) output = saturation_high_value_;     
            else if (output < saturation_low_value_) output = saturation_low_value_;     
        }

        if ((ros::Time::now()-reset_time_).toSec() < 0.5){
            return 0.0;
        }
        else{
            return output;
        }
    
    }

    
    void setBias(float bias){ bias_ = bias;}
    void setCenteredBias(){ 
        if (has_saturation_){
         bias_ = (saturation_high_value_ - saturation_low_value_)/2.0f;   
        } else {
            bias_ = 0.0f;
            std::cout << "Controller does not have saturations" << std::endl;
        }
    }

    
    void setSaturation(float low,float high) {
        has_saturation_ = true;
        saturation_high_value_ = high; 
        saturation_low_value_ = low; 
    }

    void freeSaturation(){has_saturation_ = false;}
    
    #ifdef DYNAMIC_TUNING
    void parametersCallback(BasicController::ControllerConfig &config, uint32_t level)
    {
        Kp_ = config.Kp;
        Kd_ = config.Kd;
        Ki_ = config.Ki;
        Kp_int_ = config.Kp_int;
        Kd_int_ = config.Kd_int;
        Ki_int_ = config.Ki_int;
            
    }
    #endif

};


#endif