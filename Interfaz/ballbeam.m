function ballbeam(action)

    if nargin < 1
        action = 'initialize';
    end
    % Declaramos las variables
%       rospublisher ('/runbutton','std_msgs/Int8');
%       rospublisher ('/tivalue','std_msgs/Float32');
%       rospublisher ('/tdvalue','std_msgs/Float32'); 
%       rospublisher ('/kpvalue','std_msgs/Float32');
%       rospublisher ('/modebutton','std_msgs/Int8');
%       rospublisher ('/setpointball','std_msgs/Float32');
%       rospublisher ('/anglecontrol','std_msgs/Float32');
      rossubscriber('/laser');
      rossubscriber('/angle');
      
      
    persistent ballradius beamlength beamwidth
    persistent xmin xmax umin umax g dt 
    persistent t x v u done
    persistent Kp Ti Td N xsp xlast dterm iterm Ad Bd Ai Bi
    persistent ballHndl xball yball 
    persistent beamHndl xbeam ybeam
    persistent markerHndl xmarker ymarker
    persistent setpointHndl clickrun
    persistent runHndl resetHndl stopHndl closeHndl infoHndl
    persistent gainHndl gaintxtHndl gainlblHndl
    persistent dervHndl dervtxtHndl dervlblHndl intHndl inttxtHndl intlblHndl
    persistent manualHndl modeHndl mode
    persistent bbAxes plotAxes plotHndl legHndl tdata udata xdata xspdata

    switch lower(action)
        
    case {'initialize'}

        % Basic dimensions.

        ballradius = 1;
        beamlength = 50;
        beamwidth = 0.2;

        % System parameters

        xmin = 0;                   % Left limit
        xmax = beamlength;          % Right limit
        umin = -0.7;               % Minimum beam angle
        umax = 0.7;                % Maximum beam angle
        g = 0.05;                   % Gravitational Constant
        dt = 0.2;                   % Time Step
        clickrun=1;
        % Set initial conditions for ball and beam position
        laser = rossubscriber('/laser');
        scandatalaser = receive(laser,10);
        scandatalaser.Data;
        t = 0;                      % Initial time
        x = 51-scandatalaser.Data; %beamlength/2;   % Initial Ball position
        v = 0;                      % Initial Velocity
%         angle = rossubscriber('/angle');
%         scandataangle = receive(angle,10);
%         u=scandataangle.Data*pi/180;
        %u=0;                      % Initial beam angle
        done = 0;                   % Done Flag

        % Setup controller with initial parameters

        mode = 1;                   % Flag, start in manual mode
                                    %   Mode == 1 => Manual
                                    %   Mode == 2 => Proportional
                                    %   Mode == 3 => PD
                                    %   Mode == 4 => PI
        Kp = 0.1;                   % Proportional Gain
        Td = 20;                    % Derivative time constant
        Ti=1;                       % Integral time constant
        N = 10;                     % Maximum derivative gain
        xsp = beamlength/2;         % Initial setpoint
        xlast = x;                  % Last measured value

        
        % Initialize Data stores
        
        xdata = x;
        tdata = t;
        udata = u;
        xspdata = xsp;

        
        x = beamlength/2;
        v = 0;
        u = 0;
        t = 0;
        xsp = x;
        xlast = x;
        dterm = 0;
        iterm = 0;
        xdata = x;
        tdata = t;
        udata = u;
        xspdata = xsp;
        set(setpointHndl,'Value',beamlength/2);
        set(beamHndl,'Xdata',xbeam,'Ydata',ybeam);
        set(ballHndl,'Xdata',x + xball, 'Ydata',yball);
        set(markerHndl,'Xdata',xsp + xmarker,'Ydata',ymarker);
        
        
        
        
        
        % Create figure

        figure( ...
            'Menubar','None',...
            'Name','Laboratorio de automatica y robotica', ...
            'NumberTitle','off', ...
            'BackingStore','off',...
            'CloseRequestFcn','ballbeam(''close'')');

        % Create axes to hold ball & beam animation and plot

        bbAxes = axes(...
            'Units','normalized', ...
            'Position',[0.02 0.02 0.55 0.70],...%Axis
            'Visible','off',...
            'NextPlot','add');

        plotAxes = axes(...
            'Units','normalized',...
            'Position',[0.08 0.65 0.70 0.30],...
            'Visible','off');

        % Information for all buttons
        xPos=0.85;
        btnWid=0.12;
        btnHt=0.08;
        % Spacing between the button and the next command's label
        spacing=0.025;

        %====================================
        % CONSOLE frame container for buttons
        frmBorder=0.02;
        yPos=0.05-frmBorder;
        frmPos=[xPos-frmBorder yPos btnWid+2*frmBorder 0.9+2*frmBorder];
        uicontrol( ...
            'Style','frame', ...
            'Units','normalized', ...
            'Position',frmPos, ...
            'BackgroundColor',[0.5 0.5 0.7 0.7]);

        %====================================
        % RUN button
        btnNumber=1;
        yPos=0.90-(btnNumber-1)*(btnHt+spacing);

        
      runHndl=uicontrol( ...
          'Style','pushbutton', ...
          'Units','normalized', ...
          'Position',[xPos yPos-spacing btnWid btnHt], ...
          'String','Run', ...
          'Interruptible','on', ...
          'Callback','ballbeam(''run'');');
        %====================================
        % The STOP button
        btnNumber=2;
        yPos=0.90-(btnNumber-1)*(btnHt+spacing);

        stopHndl=uicontrol( ...
            'Style','pushbutton', ...
            'Units','normalized', ...
            'Position',[xPos yPos-spacing btnWid btnHt], ...
            'Enable','off', ...
            'String','Stop', ...
            'Callback','ballbeam(''stop'');');

        %====================================
        % RESET button
        btnNumber=3;
        yPos=0.90-(btnNumber-1)*(btnHt+spacing);

        resetHndl=uicontrol( ...
          'Style','pushbutton', ...
          'Units','normalized', ...
          'Position',[xPos yPos-spacing btnWid btnHt], ...
          'String','Reset', ...
          'Interruptible','on', ...
          'Callback','ballbeam(''reset'');');

        %====================================
        % The MODE popup button
        btnNumber=4;
        yPos=0.90-(btnNumber-1)*(btnHt+spacing);
        textStr='Mode';
        popupStr=['Manual';' PID  ';'CAMARA';' VACIO'];

        % Generic button information
        btnPos1=[xPos yPos-spacing+btnHt/2 btnWid btnHt/2];
        btnPos2=[xPos yPos-spacing btnWid btnHt/2];
        uicontrol( ...
           'Style','text', ...
           'Units','normalized', ...
           'Position',btnPos1, ...
           'String',textStr, ...
           'BackgroundColor',[0.5 0.5 0.7]);
        modeHndl=uicontrol( ...
           'Style','popup', ...
           'Units','normalized', ...
           'Position',btnPos2, ...
           'String',popupStr,...
           'Value',mode,...
           'Callback','ballbeam(''mode'')');

        %====================================
        % The first slider -- PID, proportional control gain
        btnNumber=5;
        yPos=0.90-(btnNumber-1)*(btnHt+spacing);

        gainHndl = uicontrol( ...
            'Style','slider', 'Units','normalized', ...
            'Position',[xPos yPos-btnHt btnWid btnHt/2], ...
            'Min',0, ...
            'Max', 1, ...
            'Value', Kp, ...
            'Visible','off',...
            'Callback','ballbeam(''control'');');
        gainlblHndl = uicontrol( ...
            'Style','text', ...
            'String','Kp', ...
            'Units','normalized', ...
            'Visible','off',...
            'Position',[xPos yPos btnWid btnHt/2]);
        gaintxtHndl = uicontrol( ...
            'Style','text', ...
            'String',num2str(Kp), ...
            'Units','normalized', ...
            'Visible','off',...
            'Position',[xPos yPos-btnHt/2 btnWid btnHt/2]);
       % The second slider -- Ti, the Integrer Time Constant
             btnNumber=7;
        yPos=0.90-(btnNumber-2)*(btnHt+spacing);

        intHndl = uicontrol( ...
            'Style','slider', ...
            'Units','normalized', ...
            'Position',[xPos yPos-1.5*btnHt btnWid btnHt/2], ...
            'Min',0, ...
            'Max',50, ...
            'Value',Ti,...
            'Visible','off',...
            'Callback','ballbeam(''control'')');
        intlblHndl = uicontrol( ...
            'Style','text', ...
            'String','Ti', ...
            'Units','normalized', ...
            'Visible','off',...
            'Position',[xPos yPos-btnHt/2 btnWid btnHt/2]);
        inttxtHndl = uicontrol( ...
            'Style','text', ...
            'String',num2str(Ti), ...
            'Units','normalized', ...
            'Visible','off',...
            'Position',[xPos yPos-btnHt btnWid btnHt/2]);

       btnNumber=6;
        yPos=0.90-(btnNumber-1)*(btnHt+spacing);

        dervHndl = uicontrol( ...
            'Style','slider', ...
            'Units','normalized', ...
            'Position',[xPos -0.125+(yPos-1.5*btnHt) btnWid btnHt/2], ...
            'Min',0, ...
            'Max',50, ...
            'Value',Td,...
            'Visible','off',...
            'Callback','ballbeam(''control'')');
        dervlblHndl = uicontrol( ...
            'Style','text', ...
            'String','Td', ...
            'Units','normalized', ...
            'Visible','off',...
            'Position',[xPos -0.125+(yPos-btnHt/2) btnWid btnHt/2]);
        dervtxtHndl = uicontrol( ...
            'Style','text', ...
            'String',num2str(Td), ...
            'Units','normalized', ...
            'Visible','off',...
            'Position',[xPos -0.125+(yPos-btnHt) btnWid btnHt/2]);
      
        
        if mode == 2,
            set(gainlblHndl,'Visible','on');
            set(gaintxtHndl,'Visible','on');
            set(intlblHndl,'Visible','on');
            set(inttxtHndl,'Visible','on');
            set(dervlblHndl,'Visible','on');
            set(dervtxtHndl,'Visible','on');
        end
         if mode == 3,
            set(gainlblHndl,'Visible','off');
            set(gaintxtHndl,'Visible','off');
            set(intlblHndl,'Visible','off');
            set(inttxtHndl,'Visible','off');
            set(dervlblHndl,'Visible','off');
            set(dervtxtHndl,'Visible','off');
        end

        %====================================
        % The CLOSE button

        closeHndl=uicontrol( ...
            'Style','push', ...
            'Units','normalized', ...
            'Position',[xPos 0.05 btnWid btnHt], ...
            'String','Close & Plot', ...
            'Callback','ballbeam(''close'')');
        
        % Initialize animation axes

        axes(bbAxes);

        % Create Ball, keep handle
        a = 2*pi*(0:.05:.95)';
        xball = ballradius*sin(a);
        yball = ballradius*(1+cos(a));
        ballHndl = patch(xball+x,yball,1);

        % Create Beam
        xbeam = beamlength*[0 1 1 0]';
        ybeam = beamwidth*[0 0 -1 -1]';
        beamHndl = patch(xbeam,ybeam,2);

        % Create Fulcrum
        xpivot = [0 1 -1 0]';
        ypivot = [-beamwidth -5 -5 -beamwidth]';
        patch(xpivot,ypivot,3);

        % Create Setpoint Marker   
        xmarker = 3*beamwidth*[0 1 -1]';
        ymarker = 3*beamwidth*[0 -1 -1]'-beamwidth;
        markerHndl = patch(xmarker + xsp,ymarker,'r');

        % Draw

        axis equal
        axis([-1-2*ballradius, beamlength+2*ballradius, -20, 20 + ballradius]);

        % Create setpoint slider
     
        setpointHndl = uicontrol(gcf, ...
            'Units','normalized',...
            'Position',[0.05 0.05 0.75 0.04],...
            'Style','slider', ...
            'Min',0, ...
            'Max', beamlength, ...
            'Value', beamlength/2,...
            'Callback','ballbeam(''setpoint'')');
        
    
        % Create manual control slider

        manualHndl = uicontrol(gcf, ...
            'Units','normalized',...
            'Position',[0.78 0.12 0.03 0.50],...
            'Style','slider',...
            'Min',umin,...
            'Max',umax,...
            'Visible','off',...
            'Value',0,...
            'Callback','ballbeam(''manual'')');
        if mode == 1,
            set(manualHndl,'Visible','on');
        end

        drawnow;

    case {'run'}
        clickrun=0;
        done = 0;
        %Publisher Runbutton
        runbutton = rospublisher ('/runbutton','std_msgs/Int8');
        msgrunbutton = rosmessage(runbutton);
        msgrunbutton.Data = done;
        send(runbutton,msgrunbutton);
        
        set([runHndl,resetHndl,closeHndl,infoHndl],'Enable','off');
        set(stopHndl,'Enable','on');
        set(plotAxes,'Visible','off');
        if ishandle(plotHndl)
            set(plotHndl,'Visible','off');
        end
        if ishandle(legHndl)
            set(legHndl,'Visible','off');
        end
        while (x > xmin) && (x < xmax) && (~done),

            % Update controller using current ball position
            % Last value of the control is in u
            % Current value of the state is in x

            if mode == 2 %|| mode == 2 || mode == 3  || mode == 4
                flagmodocontrol=2;
                modebutton = rospublisher ('/modebutton','std_msgs/Int8');
                msgmodebutton = rosmessage(modebutton);
                msgmodebutton.Data = flagmodocontrol;
                send(modebutton,msgmodebutton);        
                
                angle = rossubscriber('/angle');
                scandataangle = receive(angle,10);
                u=scandataangle.Data*pi/180;
            end
            if mode == 3 
                flagmodocontrol=3;
                modebutton = rospublisher ('/modebutton','std_msgs/Int8');
                msgmodebutton = rosmessage(modebutton);
                msgmodebutton.Data = flagmodocontrol;
                send(modebutton,msgmodebutton);        
                
                angle = rossubscriber('/angle');
                scandataangle = receive(angle,10);
                u=scandataangle.Data*pi/180;
            end
            if mode ==  1
                flagmodocontrol=1;
                modebutton = rospublisher ('/modebutton','std_msgs/Int8');
                msgmodebutton = rosmessage(modebutton);
                msgmodebutton.Data = flagmodocontrol;
                send(modebutton,msgmodebutton);
                u = get(manualHndl,'Value');
                anglecontrol = rospublisher ('/anglecontrol','std_msgs/Float32');
                msganglecontrol= rosmessage(anglecontrol);
                msganglecontrol.Data = u;
                send(anglecontrol,msganglecontrol);
            end
%             
             
            u = max(min(u,umax),umin);

            % Update velocity and position
              laser = rossubscriber('/laser');
            scandatalaser = receive(laser,10);
            scandatalaser.Data;
            xlast = scandatalaser.Data;
            laser = rossubscriber('/laser');
            scandatalaser = receive(laser,10);
            scandatalaser.Data;
            x = 51-scandatalaser.Data;
            t = t + dt;

            % Store Data

            tdata = [tdata,t];
            udata = [udata,u];
            xdata = [xdata,x];
            xspdata = [xspdata,xsp];

            % Update Ball and Beam Animation

            set(beamHndl, ...
                'Xdata',cos(u)*xbeam - sin(u)*ybeam, ...
                'Ydata',sin(u)*xbeam + cos(u)*ybeam);
            set(ballHndl, ...
                'Xdata',cos(u)*(x + xball) - sin(u)*yball, ...
                'Ydata',sin(u)*(x + xball) + cos(u)*yball);
            set(markerHndl, ...
                'Xdata',cos(u)*(xsp + xmarker) - sin(u)*ymarker, ...
                'Ydata',sin(u)*(xsp + xmarker) + cos(u)*ymarker);
            drawnow;

        end
        
        if x < xmin
            x = xmin;
            set(ballHndl,'Xdata',x - 1 - ballradius + xball,'Ydata',yball-5);
        elseif x > xmax
            x = xmax;
            set(ballHndl,'Xdata',x + ballradius + xball,'Ydata',yball-5);
        end

        % Update Plot

        axes(plotAxes);
        plotHndl = plot(tdata,xdata,'b',tdata,xspdata,'r');
        axis([min(tdata),max(tdata),xmin,xmax]);
        xlabel('Time');
        ylabel('Beam Position');
        legHndl = legend(...
            'Ball Position',...
            'Setpoint',...
            'Location','NorthWest');
        set(plotAxes,'Visible','on');
        set([runHndl,resetHndl,closeHndl,infoHndl],'Enable','on');
        set(stopHndl,'Enable','off');
        drawnow;
        
        axes(bbAxes); 
        
        % Write data to file
        save ballbeam tdata xdata xspdata udata Kp Td Ti
        
    case{'stop'}  % Set stop flag. Ends running status.
        done = 1;
        clickrun=1;
        runbutton = rospublisher ('/runbutton','std_msgs/Int8');
        msgrunbutton = rosmessage(runbutton);
        msgrunbutton.Data = done;
        send(runbutton,msgrunbutton);

    case{'manual'}   % Get the beam angle from the manual slider
        flagmodocontrol=1;
        modebutton = rospublisher ('/modebutton','std_msgs/Int8');
        msgmodebutton = rosmessage(modebutton);
        msgmodebutton.Data = flagmodocontrol;
        send(modebutton,msgmodebutton);
        
                angle = rossubscriber('/angle');
                scandataangle = receive(angle,10);
                %u = scandataangle.Data*pi/180;
         u = get(manualHndl,'Value');
        
        
        set(beamHndl, ...
            'Xdata',cos(u)*xbeam - sin(u)*ybeam, ...
            'Ydata',sin(u)*xbeam + cos(u)*ybeam);
        set(ballHndl, ...
            'Xdata',cos(u)*(x + xball) - sin(u)*yball, ...
            'Ydata',sin(u)*(x + xball) + cos(u)*yball);
        set(markerHndl, ...
            'Xdata',cos(u)*(xsp + xmarker) - sin(u)*ymarker, ...
            'Ydata',sin(u)*(xsp + xmarker) + cos(u)*ymarker);

    case('setpoint')  % Get the setpoint from the setpoint slider
        if(clickrun==0)
        xsp = get(setpointHndl,'Value');
        setpointball = rospublisher ('/setpointball','std_msgs/Float32');
        msgsetpointball = rosmessage(setpointball);
        msgsetpointball.Data = xsp-25;
        send(setpointball,msgsetpointball);
        end
        set(markerHndl, ...
            'Xdata',cos(u)*(xsp + xmarker) - sin(u)*ymarker, ...
            'Ydata',sin(u)*(xsp + xmarker) + cos(u)*ymarker);

    case{'reset'}
        clickrun=1;
        flagreset=2;
        runbutton = rospublisher ('/runbutton','std_msgs/Int8');
        msgrunbutton = rosmessage(runbutton);
        msgrunbutton.Data = flagreset;
        send(runbutton,msgrunbutton);
        
        set(plotAxes,'Visible','off');
        if ishandle(plotHndl)
            set(plotHndl,'Visible','off');
        end
        if ishandle(legHndl)
            set(legHndl,'Visible','off');
        end

        % Reset the ball to the middle and level the beam.

        x = beamlength/2;
        v = 0;
        u = 0;
        t = 0;
        xsp = x;
        xlast = x;
        dterm = 0;
        iterm = 0;
        xdata = x;
        tdata = t;
        udata = u;
        xspdata = xsp;
        set(setpointHndl,'Value',beamlength/2);
        set(beamHndl,'Xdata',xbeam,'Ydata',ybeam);
        set(ballHndl,'Xdata',x + xball, 'Ydata',yball);
        set(markerHndl,'Xdata',xsp + xmarker,'Ydata',ymarker);

    case{'mode'}  % Get control mode, adjust menu visibilities
        mode = get(modeHndl,'Value');

        if mode == 1,   % Manual Mode
            set(manualHndl,'Visible','on');
            set(gainlblHndl,'Visible','off');
            set(gaintxtHndl,'Visible','off');
            set(gainHndl,'Visible','off');
            set(dervlblHndl,'Visible','off');
            set(dervtxtHndl,'Visible','off');
            set(dervHndl,'Visible','off');
            set(manualHndl,'Value',u);
            set(intlblHndl,'Visible','off');
            set(intHndl,'Visible','off');
            set(inttxtHndl,'Visible','off');
            set(intHndl,'Visible','off');

        elseif mode == 2,  % PID Mode
            
            set(manualHndl,'Visible','off');
            set(gainlblHndl,'Visible','on');
            set(gaintxtHndl,'Visible','on');
            set(gainHndl,'Visible','on');
            set(dervlblHndl,'Visible','on');
            set(dervtxtHndl,'Visible','on');
            set(dervHndl,'Visible','on');
            set(intlblHndl,'Visible','on');
            set(inttxtHndl,'Visible','on');
            set(intHndl,'Visible','on');

        elseif mode == 3,  % PD Mode
            
            set(manualHndl,'Visible','off');
            set(gainlblHndl,'Visible','off');
            set(gaintxtHndl,'Visible','off');
            set(gainHndl,'Visible','off');
            set(dervlblHndl,'Visible','off');
            set(dervtxtHndl,'Visible','off');
            set(dervHndl,'Visible','off');
            set(intlblHndl,'Visible','off');
            set(inttxtHndl,'Visible','off');
            set(intHndl,'Visible','off');
            dterm = 0;
%         elseif mode == 4,  % PI Mode
%             
%             set(manualHndl,'Visible','off');
%             set(gainlblHndl,'Visible','on');
%             set(gaintxtHndl,'Visible','on');
%             set(gainHndl,'Visible','on');
%             set(dervlblHndl,'Visible','off');
%             set(dervtxtHndl,'Visible','off');
%             set(dervHndl,'Visible','off');
%             set(intlblHndl,'Visible','on');
%             set(inttxtHndl,'Visible','on');
%             set(intHndl,'Visible','on');
%             iterm = 0;
        end

    case{'control'}  % Get the control parameters from the sliders
        Kp = get(gainHndl,'Value');
        
        kpvalue = rospublisher ('/kpvalue','std_msgs/Float32');
        msgkpvalue = rosmessage(kpvalue);
        msgkpvalue.Data = Kp;
        send(kpvalue,msgkpvalue);
        
        
        
        Td = get(dervHndl,'Value');
        
        tdvalue = rospublisher ('/tdvalue','std_msgs/Float32');
        msgtdvalue = rosmessage(tdvalue);
        msgtdvalue.Data = Td;
        send(tdvalue,msgtdvalue);
        
        Ti = get(intHndl,'Value');
        
        tivalue = rospublisher ('/tivalue','std_msgs/Float32');
        msgtivalue = rosmessage(tivalue);
        msgtivalue.Data = Ti;
        send(tivalue,msgtivalue);
        
        set(gaintxtHndl,'String',num2str(Kp));
        set(dervtxtHndl,'String',num2str(Td));
        set(inttxtHndl,'String',num2str(Ti));

    case{'help'}  
        helpwin(mfilename);
        
    case{'close'}  % close application
        flagclose=3;
        clickrun=1;
        runbutton = rospublisher ('/runbutton','std_msgs/Int8');
        msgrunbutton = rosmessage(runbutton);
        msgrunbutton.Data = flagclose;
        send(runbutton,msgrunbutton);
        
        delete(gcf); 
        if length(tdata) > 1
            load ballbeam
            
            figure;
            subplot(2,1,1);
            plot(tdata,xdata,tdata,xspdata,'Linewidth',2);
            ylim([xmin,xmax]);
            xlabel('Time [sec]');
            ylabel('Beam Position [cm]');
            title('Controlled Variable and Setpoint');
            legend('Ball Position','Setpoint','Location','Northwest');
            
            subplot(2,1,2);
            plot(tdata,udata,'Linewidth',2);
            ylim([-0.75,0.75]);
            xlabel('Time [sec]');
            ylabel('Beam Angle [rad]');
            if mode == 1
                title('Manipulated Variable');
            elseif mode == 2
                title(sprintf('Last Value of Kp = %5.2f', Kp));
            elseif mode == 3
                title(sprintf('Last Value of Kp = %5.2f, Td = %5.2f', Kp, Td));
            else
                title(sprintf('Last Value of Kp = %5.2f, Ti = %5.2f', Kp, Ti));
            end
            
        end
        
    otherwise
        disp('Unknown action');
        
    end

end
ballbeam  

Modificaciones 09/05/2020-->  	 - Modificado referencia setpoint(-25 a +25)
				 - Añadido nuevo modo CAMARA seleccionable(publica float=3 en /modebutton)
				 - No se publica el SETPOINT hasta que no se pulsa RUN

iniciar rosinit:
Publica y se suscribe en:
      
      - rospublisher ('/runbutton','std_msgs/Int8');
	Indica si se ha pulsado alguno de los botones, RUN(0), STOP(1),RESET(2), CLOSE(3)
      
      - rospublisher ('/tivalue','std_msgs/Float32');
	Publica el valor de la cte Ti del slider

      - rospublisher ('/tdvalue','std_msgs/Float32');
        Publica el valor de la cte Td del slider
      
      - rospublisher ('/kpvalue','std_msgs/Float32');
        Publica el valor de la cte kp del slider

      - rospublisher ('/modebutton','std_msgs/Int8');
	 Indica si esta en modo manual(1) o PID(2) o CAMARA(3)

      - rospublisher ('/setpointball','std_msgs/Float32');
	Publica el setpoint de equilibrio elegido en el slider

      - rospublisher ('/anglecontrol','std_msgs/Float32');
	En modo manual publica el angulo de giro de la barra movido por el slider

      - rossubscriber('/laser');
	Se subscribe al topic laser y lo utiliza para dibujar la bola sobre la barra

      - rossubscriber('/angle');
	se subscribe al topic angle y lo utiliza para dibujar el ángulo de la barra en modo PID