cmake_minimum_required(VERSION 2.8.3)
set(PROJECT_NAME Vision_lab)
project(${PROJECT_NAME})

add_definitions(-DOPENCV)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  std_msgs
  sensor_msgs
  geometry_msgs
  cv_bridge
)

catkin_package(
  INCLUDE_DIRS include
  LIBRARIES ${PROJECT_NAME}
  CATKIN_DEPENDS roscpp std_msgs geometry_msgs sensor_msgs cv_bridge 
  )

find_package(OpenCV 3 REQUIRED)
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

# message(${OpenCV_VERSION})

set(CPP_PROJECT_FILES 
  src/vision_estimator.cpp
  src/ball_detector.cpp
)
set(HEADER_PROJECT_FILES
  include/vision_estimator.hpp
  include/ball_detector.hpp

)

#  declare library
add_library(${PROJECT_NAME}
  ${CPP_PROJECT_FILES}
  ${HEADER_PROJECT_FILES}
)

add_executable(${PROJECT_NAME}_main src/main.cpp)
target_link_libraries(${PROJECT_NAME}_main ${catkin_LIBRARIES} ${OpenCV_LIBS} ${PROJECT_NAME})
add_dependencies(${PROJECT_NAME}_main ${catkin_EXPORTED_TARGETS} ${OpenCV_LIBS})

