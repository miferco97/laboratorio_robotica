#ifndef __BALL_DETECTOR_H__
#define __BALL_DETECTOR_H__

#include <vector>

#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"


#include <iostream>
#include <memory>
#include <string>
#include<list>
#include<vector>
#include <algorithm>


class BallDetector{
private:
	int low_H, high_H;
	int low_S, high_S;
	int low_V, high_V;
	int dilation, erosion;
	std::string window_name = "Configuration window";
    cv::Point2f center_;


public:

	BallDetector(bool configure);
	~BallDetector();
	cv::Mat threshold(cv::Mat image);
	bool detect_ball(const cv::Mat &im, cv::Point2f &center, float &radius);
	int process_frame(cv::Mat frame);
	cv::Point2f getCenter(){return center_;};
};

#endif
