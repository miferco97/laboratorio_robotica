#include "ros/ros.h"
#include "vision_estimator.hpp"

int main( int argc, char** argv )
{

  ros::init(argc, argv, "pepe");
  VisionEstimator v_estimator1(1);
  v_estimator1.setup();
  
  ros::Rate rate(30);
  while (ros::ok())
  {
    ros::spinOnce();
    v_estimator1.run();
    rate.sleep();
  }
}