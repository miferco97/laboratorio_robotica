#include "vision_estimator.hpp"

#define LOG(x) std::cout<<x<<std::endl
VisionEstimator::VisionEstimator(int n_robot):ballDetector_(false),n_robot_(n_robot){


	image_sub_ = nh_.subscribe("/camara/compressed",1,&VisionEstimator::imageCallback,this);
	
	std::string robot_name;
	std::string topic_name = "ball";

	std::cout << "Ball pose publisher at: " << topic_name << std::endl;
	pose_pub_  = nh_.advertise<geometry_msgs::PoseStamped>(topic_name,1);
	distance_pub_  = nh_.advertise<std_msgs::Float32>("/camera/estimation",1);

}

VisionEstimator::~VisionEstimator(){
}

void VisionEstimator::setup(){

}

void VisionEstimator::publishBallPose(){
	std_msgs::Float32 real_distance;
	geometry_msgs::PoseStamped real_pose;
	// TODO improve this 
	// auto value_x= (center_.x - 235)/(416.0f-235.0f) * 7.39 ;
	// auto value_y= center_.y * (-14.88f/(429.0f-59.0f))  + 14.88+ 2.3;

	real_pose.pose.position.x = ((center_.x-46.5f)/(520.5-46.5)-0.5f)*48;
	real_pose.pose.position.y = ((center_.y-65.5f)/(189-65.5f))*13-13;
	if(real_pose.pose.position.x>0)
		real_distance.data = 24 - std::sqrt(std::pow(real_pose.pose.position.x,2)+std::pow(real_pose.pose.position.y,2));
	else
		real_distance.data = 24 + std::sqrt(std::pow(real_pose.pose.position.x,2)+std::pow(real_pose.pose.position.y,2));

	// clip values x
	if (real_pose.pose.position.x > MAX_X)  real_pose.pose.position.x= MAX_X;  
	else if (real_pose.pose.position.x < MIN_X) real_pose.pose.position.x= MIN_X;
	
	// clip values y
	if (real_pose.pose.position.y > MAX_Y)  real_pose.pose.position.y= MAX_Y;  
	else if (real_pose.pose.position.y < MIN_Y) real_pose.pose.position.y= MIN_Y;
	
	pose_pub_.publish(real_pose);
	distance_pub_.publish(real_distance);
}

void VisionEstimator::run(){


	// std::cout << "x:" << center_.x << std::endl;
	// std::cout << "y:" << center_.y << std::endl;
	publishBallPose();
}



void VisionEstimator::imageCallback(const sensor_msgs::CompressedImage& msg)
{
	// std::cout << "new image"<<std::endl;
	try
	{
		image_ = cv::imdecode(cv::Mat(msg.data),1);//convert compressed image data to cv::Mat

		if(!image_.empty()){
			if (ballDetector_.process_frame(image_) > 0 ) // sucess
			center_ = ballDetector_.getCenter();
		}

	}
	catch (cv::Exception& e)
	{
		ROS_ERROR("Could not convert to image!");
	}
}
